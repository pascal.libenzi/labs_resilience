const express = require('express');
const cors = require('cors');

const app = express();
const port = 3002;

const customers = {
  101: { id: 101, name: 'Dark Vardor' },
  102: { id: 102, name: 'Master Yoda' },
  103: { id: 103, name: 'Luke Skywalker' },
  104: { id: 104, name: 'Leila Skywalker' },
  105: { id: 105, name: 'Chewbacca' },
  106: { id: 106, name: 'Ann Solo' },
};

app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

app.get('/customers/:customerId', (req, res) => {
  const customerId = parseInt(req.params.customerId);
  const customer = customers[customerId];

  if (customer) {
    res.json(customer);
  } else {
    res.status(404).json({ error: 'Client non trouvé' });
  }
});

app.listen(port, () => {
  console.log(`customers-backend écoute sur le port ${port}`);
});

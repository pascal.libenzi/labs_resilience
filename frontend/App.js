import React, { useEffect, useState } from 'react';
import './App.css'; 

const App = () => {
    const [orders, setOrders] = useState([]);
  
    useEffect(() => {
      // Appel du backend orders pour obtenir la liste des commandes
      fetch('http://localhost:3001/orders')
        .then(response => response.json())
        .then(ordersData => {
          // Pour chaque commande, appel du backend customers pour obtenir le nom du client
          const fetchCustomerNames = ordersData.map(order => {
            return fetch(`http://localhost:3002/customers/${order.customerId}`)
              .then(response => response.json())
              .then(customerData => ({
                orderId: order.id,
                customerName: customerData.name,
                price: order.price
              })).catch(error => {
                  console.error(error);
                  // En cas d'erreur, définir une valeur par défaut pour le nom du client
                  return {
                    orderId: order.id,
                    customerName: '<Not available now>',
                    price: order.price
                  };
                });
          });
  
          // Attendre que toutes les requêtes soient terminées
          Promise.all(fetchCustomerNames)
            .then(ordersWithCustomerNames => {
              setOrders(ordersWithCustomerNames);
            });
        });
    }, []);
  
    return (
        <div className="app-container">
        <h1>Liste des commandes</h1>
        <table className="orders-table">
          <thead>
            <tr>
              <th>Order</th>
              <th>Customer</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              <tr key={order.orderId}>
                <td>{order.orderId}</td>
                <td>{order.customerName}</td>
                <td>{order.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  };
  
  export default App;
  
import React, { useEffect, useState } from 'react';

import ReactDOM from 'react-dom';
import App from './App'; // Assurez-vous que le chemin est correct

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


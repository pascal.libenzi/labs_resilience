sed -i "s/localhost/$HOSTNAME.labs.sonative.io/g" frontend/App.js

cd customers && docker build . -t customers:0.0.1-alpha && cd ..
cd orders && docker build . -t orders:0.0.1-alpha && cd ..
cd frontend && docker build . -t frontend:0.0.1-alpha && cd ..
docker-compose up

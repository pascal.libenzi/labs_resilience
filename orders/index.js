const express = require('express');
const cors = require('cors');

const app = express();
const port = 3001;

const orders = [
  { id: 1, customerId: 101, price: 2000 },
  { id: 2, customerId: 102, price: 3333 },
  { id: 3, customerId: 103, price: 2500 },
  { id: 4, customerId: 102, price: 4000 },
  { id: 5, customerId: 104, price: 6000 },
  { id: 6, customerId: 105, price: 20 },
];

app.use(cors({
  origin: '*', 
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
}));

app.get('/orders', (req, res) => {
  res.json(orders);
});

app.listen(port, () => {
  console.log(`orders-backend écoute sur le port ${port}`);
});
